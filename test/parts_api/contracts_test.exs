defmodule PartsApi.ContractsTest do
  use PartsApi.DataCase

  alias PartsApi.Contracts

  describe "contracts" do
    alias PartsApi.Contracts.Contract

    @valid_attrs %{date_final: "some date_final", date_init: "some date_init", file: "some file", title: "some title"}
    @update_attrs %{date_final: "some updated date_final", date_init: "some updated date_init", file: "some updated file", title: "some updated title"}
    @invalid_attrs %{date_final: nil, date_init: nil, file: nil, title: nil}

    def contract_fixture(attrs \\ %{}) do
      {:ok, contract} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Contracts.create_contract()

      contract
    end

    test "list_contracts/0 returns all contracts" do
      contract = contract_fixture()
      assert Contracts.list_contracts() == [contract]
    end

    test "get_contract!/1 returns the contract with given id" do
      contract = contract_fixture()
      assert Contracts.get_contract!(contract.id) == contract
    end

    test "create_contract/1 with valid data creates a contract" do
      assert {:ok, %Contract{} = contract} = Contracts.create_contract(@valid_attrs)
      assert contract.date_final == "some date_final"
      assert contract.date_init == "some date_init"
      assert contract.file == "some file"
      assert contract.title == "some title"
    end

    test "create_contract/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contracts.create_contract(@invalid_attrs)
    end

    test "update_contract/2 with valid data updates the contract" do
      contract = contract_fixture()
      assert {:ok, %Contract{} = contract} = Contracts.update_contract(contract, @update_attrs)
      assert contract.date_final == "some updated date_final"
      assert contract.date_init == "some updated date_init"
      assert contract.file == "some updated file"
      assert contract.title == "some updated title"
    end

    test "update_contract/2 with invalid data returns error changeset" do
      contract = contract_fixture()
      assert {:error, %Ecto.Changeset{}} = Contracts.update_contract(contract, @invalid_attrs)
      assert contract == Contracts.get_contract!(contract.id)
    end

    test "delete_contract/1 deletes the contract" do
      contract = contract_fixture()
      assert {:ok, %Contract{}} = Contracts.delete_contract(contract)
      assert_raise Ecto.NoResultsError, fn -> Contracts.get_contract!(contract.id) end
    end

    test "change_contract/1 returns a contract changeset" do
      contract = contract_fixture()
      assert %Ecto.Changeset{} = Contracts.change_contract(contract)
    end
  end
end
