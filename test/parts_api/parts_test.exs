defmodule PartsApi.PartsTest do
  use PartsApi.DataCase

  alias PartsApi.Parts

  describe "parts" do
    alias PartsApi.Parts.Part

    @valid_attrs %{cpf: 42, email: "some email", last_name: "some last_name", name: "some name", phone: 42}
    @update_attrs %{cpf: 43, email: "some updated email", last_name: "some updated last_name", name: "some updated name", phone: 43}
    @invalid_attrs %{cpf: nil, email: nil, last_name: nil, name: nil, phone: nil}

    def part_fixture(attrs \\ %{}) do
      {:ok, part} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Parts.create_part()

      part
    end

    test "list_parts/0 returns all parts" do
      part = part_fixture()
      assert Parts.list_parts() == [part]
    end

    test "get_part!/1 returns the part with given id" do
      part = part_fixture()
      assert Parts.get_part!(part.id) == part
    end

    test "create_part/1 with valid data creates a part" do
      assert {:ok, %Part{} = part} = Parts.create_part(@valid_attrs)
      assert part.cpf == 42
      assert part.email == "some email"
      assert part.last_name == "some last_name"
      assert part.name == "some name"
      assert part.phone == 42
    end

    test "create_part/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Parts.create_part(@invalid_attrs)
    end

    test "update_part/2 with valid data updates the part" do
      part = part_fixture()
      assert {:ok, %Part{} = part} = Parts.update_part(part, @update_attrs)
      assert part.cpf == 43
      assert part.email == "some updated email"
      assert part.last_name == "some updated last_name"
      assert part.name == "some updated name"
      assert part.phone == 43
    end

    test "update_part/2 with invalid data returns error changeset" do
      part = part_fixture()
      assert {:error, %Ecto.Changeset{}} = Parts.update_part(part, @invalid_attrs)
      assert part == Parts.get_part!(part.id)
    end

    test "delete_part/1 deletes the part" do
      part = part_fixture()
      assert {:ok, %Part{}} = Parts.delete_part(part)
      assert_raise Ecto.NoResultsError, fn -> Parts.get_part!(part.id) end
    end

    test "change_part/1 returns a part changeset" do
      part = part_fixture()
      assert %Ecto.Changeset{} = Parts.change_part(part)
    end
  end
end
