defmodule PartsApi.Repo.Migrations.CreateParts do
  use Ecto.Migration

  def change do
    create table(:parts) do
      add :name, :string
      add :last_name, :string
      add :email, :string
      add :cpf, :string
      add :phone, :string

      timestamps()
    end   
  end
end
