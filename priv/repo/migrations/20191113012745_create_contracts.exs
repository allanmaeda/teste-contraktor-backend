defmodule PartsApi.Repo.Migrations.CreateContracts do
  use Ecto.Migration

  def change do
    create table(:contracts) do
      add :title, :string
      add :date_init, :string
      add :date_final, :string
      add :file, :string

      timestamps()
    end
  end
end
