defmodule PartsApi.Repo.Migrations.UpdatePartsTable do
  use Ecto.Migration

  def change do
    alter table(:parts) do
      add :contract_id, references(:contracts)
    end
  end
end
