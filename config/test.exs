use Mix.Config

# Configure your database
config :parts_api, PartsApi.Repo,
  username: "postgres",
  password: "postgres",
  database: "parts_api_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :parts_api, PartsApiWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
