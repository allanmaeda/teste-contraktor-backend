# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :parts_api,
  ecto_repos: [PartsApi.Repo]

# Configures the endpoint
config :parts_api, PartsApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "DF3X7pAfrZUW558DAyBULrb1PzoR+TZ1OZ1GcdEWeNILjbtWN5g47MPRbGoA95mj",
  render_errors: [view: PartsApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: PartsApi.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
