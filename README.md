# PartsApi

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Configure your database credentials in config/dev.ex 
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

  ## Routes

  * /api/parts context of parts from contract.
  * /api/contracts context of contract.
  If you want visualize this routes in your terminal, you can type: `mix phx.routes`
