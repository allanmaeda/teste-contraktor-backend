defmodule PartsApiWeb.Router do
  use PartsApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", PartsApiWeb do
    pipe_through :api
    resources "/parts", PartController, except: [:new, :edit]
    resources "/contracts", ContractController, except: [:new, :edit]
  end
end
