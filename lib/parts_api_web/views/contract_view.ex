defmodule PartsApiWeb.ContractView do
  use PartsApiWeb, :view
  alias PartsApiWeb.ContractView

  def render("index.json", %{contracts: contracts}) do
    %{data: render_many(contracts, ContractView, "contract.json")}
  end

  def render("show.json", %{contract: contract}) do
    %{data: render_one(contract, ContractView, "contract.json")}
  end

  def render("contract.json", %{contract: contract}) do
    %{id: contract.id,
      title: contract.title,
      date_init: contract.date_init,
      date_final: contract.date_final,
      file: contract.file}
  end
end
