defmodule PartsApiWeb.PartView do
  use PartsApiWeb, :view
  alias PartsApiWeb.PartView

  def render("index.json", %{parts: parts}) do
    %{data: render_many(parts, PartView, "part.json")}
  end

  def render("show.json", %{part: part}) do
    %{data: render_one(part, PartView, "part.json")}
  end

  def render("part.json", %{part: part}) do
    %{id: part.id,
      name: part.name,
      last_name: part.last_name,
      email: part.email,
      cpf: part.cpf,
      phone: part.phone,
      contract_id: part.contract}
  end
end
