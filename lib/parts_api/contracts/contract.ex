defmodule PartsApi.Contracts.Contract do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contracts" do
    field :date_final, :string
    field :date_init, :string
    field :file, :string
    field :title, :string

    has_many :parts, PartsApi.Parts.Part

    timestamps()
  end

  @doc false
  def changeset(contract, attrs) do
    contract
    |> cast(attrs, [:title, :date_init, :date_final, :file])
    |> validate_required([:title, :date_init, :date_final, :file])
  end
end
