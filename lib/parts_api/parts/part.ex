defmodule PartsApi.Parts.Part do
  use Ecto.Schema
  import Ecto.Changeset

  schema "parts" do
    field :cpf, :string
    field :email, :string
    field :last_name, :string
    field :name, :string
    field :phone, :string

    belongs_to :contract, PartsApi.Contracts.Contract

    timestamps()
  end

  @doc false
  def changeset(part, attrs) do
    part
    |> cast(attrs, [:name, :last_name, :email, :cpf, :phone])
    |> validate_required([:name, :last_name, :email, :cpf, :phone])
  end
end
