defmodule PartsApi.Repo do
  use Ecto.Repo,
    otp_app: :parts_api,
    adapter: Ecto.Adapters.Postgres
end
